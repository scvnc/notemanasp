﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.UI.WebControls;
using System.Collections;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Data.SqlClient;
using Noteman;


public partial class _Default : Page
{
    private List<Note> queryAllNotes(params object[] list)
    {
        return this.NStore.getNotes(0);
    }


    private List<Note> queryByTag(params object[] list)
    {
        return this.NStore.getNotesByTag(this.searchBox.Text);

    }
    private delegate List<Note> NoteQuery(params object[] list);
    private NoteQuery _nqf; // NoteQuery function
    private NoteQuery noteQuery
    {
        get { return _nqf; }
        set
        {
            _nqf = value;
            //noteCountLabelValue.Text = Noteman.;
            updateDG();
        }
    }

    INoteStore NStore = new SQLNoteStore();
    private void updateDG()
    {

        // Get the notes and stick it in a binding list.
        var a = new BindingList<Note>(noteQuery());

        // Give the binding list to the data grid.
        this.noteListDG.DataSource = a;

    }
    protected void Page_Load(object sender, EventArgs e)
    {
        noteCountLabelValue.Text = noteListDG.Rows.Count.ToString();
        visibleClock.Text = DateTime.Now.ToLongTimeString();

        var tags = NStore.getTags();

        foreach (string tag in tags)
        {
           searchBox.Items.Add(tag);
        }
        
    }

    protected void noteListDG_RowCommand(object sender, GridViewCommandEventArgs e)
    {
       
        if (e.CommandName == "Select")
        {
            Int16 rowNumber = Convert.ToInt16(e.CommandArgument);

            noteTitle.Text = noteListDG.Rows[rowNumber].Cells[2].Text;

            noteBody.Text = noteListDG.Rows[rowNumber].Cells[3].Text;

            createTime.Text = noteListDG.Rows[rowNumber].Cells[4].Text;

            modTime.Text = noteListDG.Rows[rowNumber].Cells[5].Text;

            noteTagList.Items.Add(noteListDG.Rows[rowNumber].Cells[6].Text);
        }
    }
    protected void timer_Clock_Tick(object sender, EventArgs e)
    {
        visibleClock.Text = DateTime.Now.ToLongTimeString();
    }
    protected void searchBox_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (Regex.Match(this.searchBox.Text, @"^\s*$").Success)
            this.noteQuery = queryAllNotes;
        else
            this.noteQuery = queryByTag;
    }
    protected void searchBox_TextChanged(object sender, EventArgs e)
    {
        if (Regex.Match(this.searchBox.Text, @"^\s*$").Success)
            this.noteQuery = queryAllNotes;
        else
            this.noteQuery = queryByTag;
    }
}