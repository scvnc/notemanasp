﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddNote.aspx.cs" Inherits="Add_Note" MasterPageFile="~/NotemanASP.master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="Server">
    <div>
        <p style="width: 570px"><asp:Label ID="titleLabel" runat="server" Text="Title" Font-Size="Large" 
                Font-Bold="True"></asp:Label></p>
            <asp:TextBox ID="noteTitle" runat="server" Width="570px" Height="31px" Font-Bold="True"
                Font-Size="Large" BorderStyle="Ridge"></asp:TextBox>
        <p style="width: 570px">
            <asp:Label ID="bodyLabel" runat="server" Text="Body" Font-Size="Large" 
                Font-Bold="True"></asp:Label>
        </p>
            <asp:TextBox ID="noteBody" runat="server" Height="187px" Width="570px" BorderColor="Black"
                BorderStyle="Double" Font-Size="Medium" TextMode="MultiLine"></asp:TextBox>
        <p style="width: 570px; height: 36px;">
            <asp:Label ID="tagLable" runat="server" Text="Tags" Font-Bold="True" 
                Font-Size="Large"></asp:Label>
            <asp:TextBox ID="noteTag" runat="server" Width="216px" Height="31px" Font-Bold="True"
                Font-Size="Large" BorderStyle="Ridge"></asp:TextBox>
            <asp:Button ID="addTagButton" runat="server" Text="Add" Width="130px" 
                onclick="addTagButton_Click" Height="31px" />
            <asp:Button ID="delTagButton" runat="server" Text="Delete" Width="130px" 
                Height="31px" onclick="delTagButton_Click" />
        </p>
        <p style="width: 570px; height: 36px;">
            <asp:Button ID="addNewNote" runat="server" Text="Add" Width="570px" 
                Height="35px" onclick="addNewNote_Click" />
        </p>
    </div>
</asp:Content>