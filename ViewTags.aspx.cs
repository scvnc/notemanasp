﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Noteman;

public partial class ViewTags : System.Web.UI.Page
{
    
    INoteStore NStore = new SQLNoteStore();
    protected void Page_Load(object sender, EventArgs e)
    {

        ListBox_Tags.Attributes.Add("onclick", "flashNotesTag()");

        var tags = NStore.getTags();
    
        foreach (string tag in tags)
        {
            ListBox_Tags.Items.Add(tag);
        }

        ListBox_Tags.DataBind();
    }

    protected void go_flash(object sender, EventArgs e)
    {

        tagname.Text = ListBox_Tags.SelectedValue;
    }
}