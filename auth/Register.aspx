﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NotemanASP.master" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="auth_Register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Main" Runat="Server">
    
    <table>
    
        <tr>
            <td>First Name:</td>
            <td>
                <asp:TextBox ID="FirstName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
                    ControlToValidate="FirstName" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td>Last Name:</td>
            <td>
                <asp:TextBox ID="LastName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
                    ControlToValidate="LastName" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td>Usermame:</td>
            <td>
                <asp:TextBox ID="Username" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                    ControlToValidate="Username" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td>Password:</td>
            <td>
                <asp:TextBox ID="Password" runat="server" TextMode="Password"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="Password" ErrorMessage="Required"></asp:RequiredFieldValidator>
            </td>
        </tr>

        <tr>
            <td>Credit Card:</td>
            <td>
                <asp:TextBox ID="CC" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="CCValidator" runat="server" 
                    ErrorMessage="Invalid CC #" ValidationExpression="([0-9] ?){16}" 
                    ControlToValidate="CC"></asp:RegularExpressionValidator>
            </td>
        </tr>
    
    </table>

    <asp:Button ID="Button1" runat="server" Text="Register" />

</asp:Content>

