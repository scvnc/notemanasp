﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" MasterPageFile="~/NotemanASP.master" Debug="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Main" Runat="Server">

        <asp:Login ID="loginControl" runat="server" BackColor="#F7F6F3" 
            BorderColor="#E6E2D8" BorderPadding="4" BorderStyle="Solid" BorderWidth="1px" 
            CssClass="form-signin" Font-Names="Verdana" Font-Size="0.8em" 
            ForeColor="#333333" onauthenticate="ProcessAuthReq" 
    CreateUserText="Register" CreateUserUrl="./Register.aspx" Width="256px">
            <InstructionTextStyle Font-Italic="True" ForeColor="Black" />
            <LoginButtonStyle BackColor="#FFFBFF" BorderColor="#CCCCCC" BorderStyle="Solid" 
                BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284775" />
            <TextBoxStyle Font-Size="0.8em" />
            <TitleTextStyle BackColor="#5D7B9D" Font-Bold="True" Font-Size="0.9em" 
                ForeColor="White" />
        </asp:Login>
    
</asp:Content>
