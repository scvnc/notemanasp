﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Security;




public partial class auth_Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        // Process registration if the user has posted information to this page.
        if (IsPostBack)
            processRegistrationForm();

    }


    private string generateSalt()
    {
        byte[] salt = new byte[25];

        var saltGen = RandomNumberGenerator.Create();
        saltGen.GetBytes(salt);

        return System.Text.Encoding.UTF8.GetString(salt);
    }

    private void processRegistrationForm()
    {

        // Secure Credit card information.
        string CypherCC, Key, IV;

        using (var encryptor = new CCEncryptor(Aes.Create()))
        {

            CypherCC = encryptor.Encrypt(CC.Text);

            Key = encryptor.Key;
            IV = encryptor.IV;

        }

        // Insert values into DB.
        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["notebase9"].ConnectionString))
        {

            dbConn.Open();

            // Use stored procedure
            SqlCommand query = new SqlCommand("InsertCustomer", dbConn);
            query.CommandType = System.Data.CommandType.StoredProcedure;

            query.Parameters.Add(new SqlParameter("@lname", LastName.Text));
            query.Parameters.Add(new SqlParameter("@fname", FirstName.Text));
            query.Parameters.Add(new SqlParameter("@username", Username.Text));
            query.Parameters.Add(new SqlParameter("@password", Password.Text));
            query.Parameters.Add(new SqlParameter("@salt", generateSalt()));
            query.Parameters.Add(new SqlParameter("@CreditCard", CypherCC));
            query.Parameters.Add(new SqlParameter("@EncKey", Key));
            query.Parameters.Add(new SqlParameter("@IV", IV));


            int rowsAffected = query.ExecuteNonQuery();

            if (rowsAffected != 1)
                throw new Exception("An registration error has occurred.");


        }

        FormsAuthentication.RedirectFromLoginPage(Username.Text, false);

    }

}
