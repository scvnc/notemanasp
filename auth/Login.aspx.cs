﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Data.SqlClient;
using System.Configuration;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.Identity.IsAuthenticated)
            FormsAuthentication.SignOut();
    }



    protected void ProcessAuthReq(object sender, AuthenticateEventArgs e)
    {


        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["notebase9"].ConnectionString))
        {

            dbConn.Open();

            // Use stored procedure
            SqlCommand query = new SqlCommand("ValidateCredentials", dbConn);
            query.CommandType = System.Data.CommandType.StoredProcedure;

            query.Parameters.Add( new SqlParameter("@username", loginControl.UserName));
            query.Parameters.Add( new SqlParameter("@password", loginControl.Password));

            int validLogin = (int) query.ExecuteScalar();

            if (validLogin == 1)
                FormsAuthentication.RedirectFromLoginPage( loginControl.UserName, loginControl.RememberMeSet );

        }

    }
}