﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Xml;

namespace Noteman
{
    class Connection
    {
        public SqlConnection OpenConnection()
        {
            string ConnectionString = "Server = 131.156.145.76; Database = notebase9; User Id = net9; Password = netword9";

            SqlConnection Connection = new SqlConnection(ConnectionString);
            try
            {
                Connection.Open();
                return Connection;
            }

            catch
            {
                throw new Exception("Connection Error");
            }

        }

        public SqlDataReader GetNotes()
        {
            SqlConnection myConnection = OpenConnection();

            SqlCommand myCommand = new SqlCommand("Select * FROM Notes", myConnection);

            SqlDataReader myReader;

            myReader = myCommand.ExecuteReader();

            return myReader;
        }

    }
}