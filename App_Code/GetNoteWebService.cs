﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
/// <summary>
/// Summary description for GetNoteWebService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

public class GetNoteWebService : System.Web.Services.WebService {

    public GetNoteWebService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    

    [WebMethod]
    public DataTable get_notes(string tag) {


        using (SqlConnection dbConn = new SqlConnection(ConfigurationManager.ConnectionStrings["notebase9"].ConnectionString))
        {

            dbConn.Open();

            SqlDataAdapter query;


            if (tag == "")
            {

                query = new SqlDataAdapter("SELECT * From Notes", dbConn);

            }

            else
            {
                // Use stored procedure
                query = new SqlDataAdapter("getNotesByTag", dbConn);
                query.SelectCommand.CommandType = CommandType.StoredProcedure;
                query.SelectCommand.Parameters.Add(new SqlParameter("@tag", tag));
            }

            DataTable t = new DataTable("Notes");

            query.Fill(t);

            return t;

        }



    }
    
}
