﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.IO;
using System.Text;

/// <summary>
/// Used to easily encrypt and decrypt a credit card number
/// with a specified IV and Key.
/// </summary>
public class CCEncryptor : IDisposable
{

    /// <summary>
    /// Maintains the instance of the Symmetric Algorithm being used.
    /// </summary>
    private SymmetricAlgorithm sa;

    #region Properties


    /// <summary>
    /// Gets or sets the plaintext CC no.
    /// </summary>
    public string PlaintextCC
    {
        get { return _plaintextCC; }
        set
        {

            _plaintextCC = normalizeCC(value);

        }
    }
    private string _plaintextCC = null;


    private byte[] _encryptedCC = null;

    /// <summary>
    /// Gets or sets the encrypted credit card result (Base64)
    /// </summary>
    public string EncryptedCC
    {
        get
        {
            if (_encryptedCC == null)
                return null;

            else
                return Convert.ToBase64String(_encryptedCC);
        }

        set
        {
            if (value == null)
                _encryptedCC = null;

            else
                _encryptedCC = Convert.FromBase64String(value);
        }

    }
    public byte[] EncryptedCCBytes
    {
        get { return _encryptedCC; }
        set { _encryptedCC = value; }
    }


    /// <summary>
    /// Returns the current key in use by the symmetric algorithm (in Base64)
    /// </summary>
    public string Key
    {
        get
        {
            return Convert.ToBase64String(sa.Key);
        }

        set
        {
            sa.Key = Convert.FromBase64String(value);
        }
    }

    /// <summary>
    /// Returns the current IV in use by the symmetric algorithm (in Base64)
    /// </summary>
    public string IV
    {
        get
        {
            return Convert.ToBase64String(sa.IV);
        }

        set
        {
            sa.IV = Convert.FromBase64String(value);
        }
    }

    #endregion

    /// <summary>
    /// Initializes the credit card encryptor.
    /// </summary>
    /// <param name="sa">An instance of the SymmetricAlgorithm to be used.</param>
    public CCEncryptor(SymmetricAlgorithm sa)
    {
        this.sa = sa;
    }

    /// <summary>
    /// Encrypts a string.
    /// </summary>
    /// <returns>The encrypted string.</returns>
    public string Encrypt(string cc)
    {
        PlaintextCC = cc;
        return Encrypt();
    }

    /// <summary>
    /// Encrypts the string stored in the PlaintextCC property.
    /// </summary>
    /// <returns>The encrypted string.</returns>
    public string Encrypt()
    {

        // Initialize a memory stream for managing encrypted bytes.
        using (MemoryStream byteSpace = new MemoryStream())
        {

            using (CryptoStream cryptoStream = new CryptoStream(byteSpace, sa.CreateEncryptor(), CryptoStreamMode.Write))
            {
                cryptoStream.Write(Encoding.UTF8.GetBytes(_plaintextCC), 0, _plaintextCC.Length);
            }


            _encryptedCC = byteSpace.ToArray();

            return EncryptedCC;

        }


    }

    /// <summary>
    /// Decrypts and encrypted string.
    /// </summary>
    /// <param name="encCC">Ciphertext</param>
    /// <returns>The decrypted string.</returns>
    public string Decrypt(string encCC)
    {
        EncryptedCC = encCC;
        return Decrypt();
    }


    /// <summary>
    /// Decrypts the string stored in the PlaintextCC property.
    /// </summary>
    /// <returns>The decrypted string.</returns>
    public string Decrypt()
    {

        byte[] plaintext = new byte[_encryptedCC.Length];

        // Initialize a memory stream for reading encrypted bytes.
        using (MemoryStream byteSpace = new MemoryStream(_encryptedCC))
        {

            using (CryptoStream cryptoStream = new CryptoStream(byteSpace, sa.CreateDecryptor(), CryptoStreamMode.Read))
            {
                cryptoStream.Read(plaintext, 0, _encryptedCC.Length);
            }

        }

        PlaintextCC = Encoding.UTF8.GetString(plaintext);

        return PlaintextCC;

    }



    /// <summary>
    /// Normalizes a CC Number.
    /// </summary>
    /// <param name="value">String of a credit card number.</param>
    /// <returns>String of a normalized credit card number (Ex: 1234567890123453)</returns>
    private string normalizeCC(string value)
    {
        return value.Replace(" ", "");
    }


    /// <summary>
    /// Cleans up resources.
    /// Zeros out the symmetric algorithm which may contain sensitive information.
    /// </summary>
    public void Dispose()
    {
        // Zero out the memory (which may contain IV keys.)
        sa.Clear();

    }

}