﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default"
    MasterPageFile="~/NotemanASP.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Main" runat="Server">
    <div>
        <p class="style1">
            <asp:Button ID="turnNotesOnButton" runat="server" Text="Turn Notes On" 
                Width="165px" Height="30px" />
            <asp:TextBox ID="textBox_TimeInterval" runat="server" Width="36px" 
                Height="30px"></asp:TextBox>
            Tag Search
            <asp:DropDownList ID="searchBox" runat="server" Height="30px" Width="117px" 
                onselectedindexchanged="searchBox_SelectedIndexChanged" 
                ontextchanged="searchBox_TextChanged">
            </asp:DropDownList>
            <asp:Label ID="noteCountLabelValue" runat="server" Text="NoteCount"></asp:Label>
            &nbsp;&nbsp;&nbsp;
            <asp:Label ID="visibleClock" runat="server" Text="Visible Clock"></asp:Label>
        </p>
        <p style="width: 570px"><asp:Label ID="titleLabel" runat="server" Text="Title" Font-Size="Large" 
                Font-Bold="True"></asp:Label></p>
        <p style="width: 570px">
            <asp:TextBox ID="noteTitle" runat="server" Width="570px" Height="31px" Font-Bold="True"
                Font-Size="Large" BorderStyle="Ridge"></asp:TextBox>
        </p>
        <p style="border: thin dashed #808080; width: 570px; height: 28px;">
            <asp:Label Style="float: left;" ID="createTime" runat="server" Text="Created"></asp:Label>
            <asp:Label Style="float: right;" ID="modTime" runat="server"
                Text="Modified"></asp:Label>
        </p>
        <p style="width: 570px">
            <asp:Label ID="bodyLabel" runat="server" Text="Body" Font-Size="Large" 
                Font-Bold="True"></asp:Label>
        </p>
        <p>
            <asp:TextBox ID="noteBody" runat="server" Height="187px" Width="570px" BorderColor="Black"
                BorderStyle="Double" Font-Size="Medium" TextMode="MultiLine"></asp:TextBox>
        </p>
        <p style="width: 570px; height: 36px;">
            <asp:Label ID="tagLable" runat="server" Text="Tags" Font-Bold="True" 
                Font-Size="Large"></asp:Label>
            <asp:DropDownList ID="noteTagList" runat="server" Height="25px" Width="154px">
            </asp:DropDownList>
            <asp:Button ID="addTagButton" runat="server" Text="Add" Width="72px" />
            <asp:Button ID="delTagButton" runat="server" Text="Delete" Width="72px" />
        </p>
        <div style="width: 570px; height: 384px; overflow: scroll">
            <asp:GridView ID="noteListDG" runat="server" CellPadding="4" ForeColor="#333333"
                Width="570px" AllowSorting="True" PageSize="5" AllowPaging="True" AutoGenerateColumns="False"
                DataKeyNames="NoteID" DataSourceID="SqlDataSource1" 
                OnRowCommand="noteListDG_RowCommand" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="NoteID" HeaderText="NoteID" InsertVisible="False" ReadOnly="True"
                        SortExpression="NoteID" />
                    <asp:BoundField DataField="NoteTitle" HeaderText="NoteTitle" SortExpression="NoteTitle" />
                    <asp:BoundField DataField="NoteBody" HeaderText="NoteBody" 
                        SortExpression="NoteBody" >
                    <ControlStyle Width="30px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="NoteCreation" HeaderText="NoteCreation" SortExpression="NoteCreation" />
                    <asp:BoundField DataField="NoteModification" HeaderText="NoteModification" SortExpression="NoteModification" />
                    <asp:BoundField DataField="TagName" HeaderText="TagName" SortExpression="TagName" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:notebase9ConnectionString %>"
                SelectCommand="SELECT        Notes.*, Tags.TagName
                                FROM            Notes INNER JOIN
                                NoteTag ON Notes.NoteID = NoteTag.NoteID INNER JOIN
                                Tags ON NoteTag.TagID = Tags.TagID"></asp:SqlDataSource>
        </div>
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="head">
    <style type="text/css">
        .style1
        {
            width: 570px;
            height: 30px;
        }
        .newStyle1
        {
        }
    </style>
</asp:Content>
