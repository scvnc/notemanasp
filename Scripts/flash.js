﻿var notes = []
var notePos = 0;
var interval;

function parseXml(xml) {

    notePos = 0;
    notes = [];

    xmlNotes = $(xml).find("Notes");

    if (xmlNotes.length == 0) 
    {
        $("#title").html('No notes found by tag.');
        return 0;
    }

    xmlNotes.each(function () {


        var note = { 'title': $(this).find("NoteTitle").text(),
            'body': $(this).find("NoteBody").text(),
            'mtime': $(this).find("NoteCreation").text(),
            'ctime': $(this).find("NoteModification").text(),
            'id': $(this).find("NoteID").text()
        }

        notes.push(note);

    });

    clearInterval(interval);
    interval = setInterval(switchNote, 2000);
    
}

function switchNote()
{

    $("#title").html(notes[notePos].title);
    $("#ctime").html(notes[notePos].ctime);
    $("#mtime").html(notes[notePos].mtime);
    $("#body").html(notes[notePos].body);

    if (notePos >= notes.length-1)
        notePos = 0;

    else
        notePos++;
}

function flashNotes(tag)
{

    $.ajax({
        type: "POST",
        url: "GetNoteWebService.asmx/get_notes",
        dataType: "xml",
        data: { 'tag': tag },
        success: parseXml
    });


}

function flashNotesTag()
{
    flashNotes($("#Main_ListBox_Tags")[0].value);
}


$(document).ready(function () {

    flashNotes('');

});