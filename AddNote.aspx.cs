﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Noteman;
public partial class Add_Note : System.Web.UI.Page
{
    INoteStore NStore = new SQLNoteStore();
    TagCollection Tags = new TagCollection();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void addNewNote_Click(object sender, EventArgs e)
    {
        // Prepare new note
        Note n = new Note(noteTitle.Text, noteBody.Text);

        // Insert note into the data store
        NStore.insertNote(n);

    }
    protected void addTagButton_Click(object sender, EventArgs e)
    {
        noteTag.Text = noteTag.Text.ToLower();

        Tags.Add(noteTag.Text);
    }
    protected void delTagButton_Click(object sender, EventArgs e)
    {
        Tags.Remove(noteTag.Text);
    }
}